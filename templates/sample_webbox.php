<?php
/*
Copyright 2001,2002,2003 Dashamir Hoxha, dashohoxha@users.sourceforge.net

This file is part of phpWebApp.

phpWebApp is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

phpWebApp is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with phpWebApp; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  
*/


/**
 * The PHP code of the webbox "sample_webbox".
 * @package sample-application
 */

/** The class corresponding to webbox with ID="sample_webbox" */
class sample_webbox extends WebObject
{
  /** 
   * It is called only the first time that
   * the webobj is created (only once in a session)
   * and can be used to initialize the state variables.
   * Overrides WebObject::init().
   */
  function init()
    {
    }


  /**
   * This is called by the framework to handle
   * an event that is sent to this webobject,
   * if the name of the event is 'eventName'.
   */
  function on_eventName($event)
    {
    }


  /**
   * This is called by the framework to handle
   * an event that is sent to this webobject,
   * if no on_eventName() function exists.
   * Overrides WebObject::eventHandler().
   */
  function eventHandler($event)
    {
    }


  /**
   * This is called by the framework before
   * the webobj is parsed.
   * Overrides WebObject::onParse().
   */
  function onParse()
    {
    }

  
  /**
   * This is called by the framework after
   * the webobj is parsed.
   * Overrides WebObject::afterParse().
   */
  function afterParse()
    {
    }


  /**
   * This is called by the framework before
   * the webobj is rendered. It is usually used
   * to add variables that are used in the template.
   * Overrides WebObject::onRender().
   */
  function onRender()
    {
    }
}
?>